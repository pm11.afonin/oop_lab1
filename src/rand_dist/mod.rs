pub mod empiric_dist;
pub mod mixed_dist;
pub mod std_dist;

pub use empiric_dist::*;
pub use mixed_dist::*;
pub use std_dist::*;
use rand::Rng;

fn get_rand_exl (x: f64, y: f64) -> f64 {
    let mut r: f64 = rand::thread_rng().gen_range(x..y);
    while r == x {
        println!("you're lucky");
        r = rand::thread_rng().gen_range(x..y);
    }

    r
}

pub trait RandDist { // нужно реализовать этот типаж для каждого распределения
    fn pdf (&self, x: f64) -> f64;
    fn ev (&mut self) -> f64;
    fn variance (&mut self) -> f64;
    fn assim (&mut self) -> f64;
    fn eks (&mut self) -> f64;
    fn get_rand (&self) -> f64;
    //HACK: может засунуть это в отдельный типаж или реализовать отдельно для распределений?
    fn print_hist (&self, size: u32);
}

