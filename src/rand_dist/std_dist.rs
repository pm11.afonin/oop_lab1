use crate::rand_dist::*;

//TODO: переписать повторы умножений на powi(..)?

#[derive(Debug)]
pub struct StdDist {
    mu: f64, // смещение - 0
    lamda: f64, // масштаб - 1
    nu: f64, // параметр формы
    //TODO: кэшировать a?
}

impl StdDist {
    pub fn new (mu: f64, lamda: f64, nu: f64) -> StdDist {
        if lamda <= 0. || nu <= 0. || nu >= 1. {
            panic!("Неверные входные данные для стандартного распределения");
        }
        StdDist {
            mu, lamda, nu
        }
    }

    pub fn p_big (&self) -> f64 {

        2. * self.nu.powi(2) * (3. - self.nu.powi(2)) / (3. + self.nu.powi(4))

        // 2.*self.nu.powi(2)*self.lamda * (3. - 3.*self.nu.powi(2) - self.lamda.powi(2)*self.nu.powi(4))
    }

    pub fn get_lamda (&self) -> f64 {
        self.lamda
    }
}

impl RandDist for StdDist {
    fn pdf (&self, x: f64) -> f64 {
        let x1 = ((x-self.mu)/self.lamda).abs();
        let m: f64;
        if x1 <= self.nu {
            m = 1. - x1*x1;
        } else {
            let a = 2. * self.nu / (1. - self.nu.powi(2));
            m = (1.-self.nu.powi(2)) * (-a * (x1 - self.nu)).exp();
        }

            3. * self.nu / (3. + self.nu.powi(4)) / self.lamda * m
    }
    fn ev (&mut self) -> f64 {
        0. + self.mu
    }
    fn variance (&mut self) -> f64 {
        let temp = self.nu.powi(3)/3.
                 - self.nu.powi(5)/5.
                 + (1.-self.nu.powi(2)).powi(2)*(1.+self.nu.powi(4)) /4./self.nu.powi(3);

        temp * self.lamda.powi(2) * 6. * self.nu / (3. + self.nu.powi(4)) // return
    }
    fn assim (&mut self) -> f64 {
        0.
    }
    fn eks (&mut self) -> f64 {
        let a = 2.*self.nu / (1.-self.nu.powi(2));
        let mut t = (1.-self.nu.powi(2))
                  *(24./a.powi(5)
                  + 24.*self.nu/a.powi(4)
                  + 12.*self.nu.powi(2)/a.powi(3)
                  + 4. *self.nu.powi(3)/a/a
                  +     self.nu.powi(4)/a);
        t = self.nu.powi(5)/5. - self.nu.powi(7)/7. + t;
        t = 6. * self.nu / self.variance().powi(2) / (3. + self.nu.powi(4)) * t - 3.;

        t
    }

    fn get_rand (&self) -> f64 { // моделирование
        assert_eq!(self.lamda, 1.);
        let p: f64 = self.p_big();
        let a: f64 = 2.*self.nu / (1.-self.nu.powi(2));
        let r1: f64 = get_rand_exl(0., 1.);

        if r1 <= p {
            loop {
                let r2 = get_rand_exl(0., 1.);
                let r3 = get_rand_exl(0., 1.);
                let x1 = (2.*r2-1.)*self.nu;
                if r3 <= 1.-x1.powi(2) {
                    return x1 + self.mu;
                }
            }
        } else {
            let r4 = get_rand_exl(0., 1.);
            let x2 = self.nu - r4.ln()/a;
            if r1 < (1. + p)/2. {
                return x2 + self.mu;
            } else {
                return -x2 + self.mu;
            }
        }
    }
    fn print_hist (&self, size: u32) {
        let mut sample: Vec<f64> = Vec::new(); // mutable

        for _i in 0..size {
            sample.push(self.get_rand());
        }

        let emp = EmpiricDist::new(sample);

        emp.print_hist(0);
    }
}
