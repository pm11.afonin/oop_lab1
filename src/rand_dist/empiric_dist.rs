use crate::rand_dist::*;
use rand::Rng;

#[derive(Debug)]
pub struct EmpiricDist {
    sample: Vec<f64>,
    n: u32,
    delta: f64,
    freqs: Vec<f64>,
    max_x: f64,
    min_x: f64,
    //TODO: использовать кэш
    ev_c: Option<f64>,
    variance_c: Option<f64>,
    assim_c: Option<f64>,
    eks_c: Option<f64>
}

impl EmpiricDist {
    pub fn new (mut vec: Vec<f64>) -> EmpiricDist {
        let n: usize = vec.len();
        let k: u32 = (n as f64).log2() as u32 + 1; // u32 => int
        vec.sort_unstable_by (|a, b| a.partial_cmp(b).unwrap()); //сортировка по возрастанию
        let max_x: f64 = *vec.last ().unwrap (); // последний эл
        let min_x: f64 = *vec.first ().unwrap (); // первый
        let delta: f64 = (max_x - min_x)/k as f64; // k as f64 => (double) k
        let mut freqs: Vec<f64> = Vec::with_capacity(k as usize);

        let mut seg_num: u32 = 1;
        let mut stone = min_x + seg_num as f64 *delta;
        let mut n_i: u32 = 0;

        // if n < 20 {
        //     println!("{:?}", vec);
        // }

        // свой цикл, потому что нету возможности уменьшать счётчик
        let mut i: usize = 0;
        while i < vec.len() {
            if vec[i] < stone && i != vec.len() - 1 {
                n_i += 1;
                i += 1;
            } else {
                if i == vec.len() - 1 && seg_num == k {
                    n_i += 1;
                    i += 1;
                }
                freqs.push (n_i as f64/n as f64);
                seg_num += 1;
                stone = min_x + seg_num as f64 *delta;
                n_i = 0;
            }
        }


        let freq_sum: f64 = freqs.iter().sum(); // for i..n sum += freqs[i]
        // println!("{}", freqs.len());

        // кол-во раз эта проверка помогла: I.II
        assert!((freq_sum - 1.).abs() < 0.001); // throw
        assert!(freqs.len() == k as usize);


        EmpiricDist {
            sample: vec,
            n: n as u32,
            delta,
            freqs,
            max_x,
            min_x,
            ev_c: None,
            variance_c: None,
            assim_c: None,
            eks_c: None,
        }
    }
}

impl RandDist for EmpiricDist {
    fn pdf (&self, x: f64) -> f64 {
        let max_x: f64 = *self.sample.last ().unwrap ();
        let min_x: f64 = *self.sample.first ().unwrap ();


        if x < min_x || x > max_x {
            return 0.;
        }

        // println!("{:#?}", self);

        let seg_num: usize = ((x - min_x)/self.delta) as usize;

        // TODO: зачем это??
        if seg_num >= self.freqs.len () {
            return self.freqs[seg_num - 1]/self.delta;
        }

        self.freqs[seg_num as usize]/self.delta
    }
    fn ev (&mut self) -> f64 {
        if let Some(val) = self.ev_c {
            return val;
        }
        let mut sum: f64 = 0.;
        for i in &self.sample {
            sum += *i;
        }
        self.ev_c = Some(sum/self.n as f64); // убрать Some
        self.ev_c.unwrap()
    }
    fn variance (&mut self) -> f64 { // дисперсия
        if let Some(val) = self.variance_c {
            return val;
        }
        let mut sum: f64 = 0.;
        for i in self.sample.clone() { //
            sum += (i - self.ev()).powi(2);
        }
        self.variance_c = Some(sum/self.n as f64);
        self.variance_c.unwrap()
    }

    fn assim (&mut self) -> f64 {
        if let Some(val) = self.assim_c {
            return val;
        }
        let mut sum: f64 = 0.;
        for i in self.sample.clone() {
            sum += (i - self.ev()).powi(3);
        }

        self.assim_c = Some(sum/self.n as f64 / self.variance().powf(1.5));
        self.assim_c.unwrap() // без unwrap
    }
    fn eks (&mut self) -> f64 {
        if let Some(val) = self.eks_c {
            return val;
        }
        let mut sum: f64 = 0.;
        for i in self.sample.clone() {
            sum += (i - self.ev()).powi(4);
        }

        self.eks_c = Some(sum/self.n as f64/self.variance().powi(2) - 3.);
        self.eks_c.unwrap()
    }

    fn get_rand (&self) -> f64 {
        // FIXME: здесь точно что-то неправильно. гистограмма смещена вправо
        let r = get_rand_exl (0., 1.);

        let mut store: f64 = 0.;
        let vec: Vec<f64> = self.freqs.iter().map(|x| {store += x; store}).collect(); // в методичке к первой лабе

        for i in 0..vec.len() {
            if r < vec[i] || r == vec[vec.len()-1] {
                if i == vec.len()-1 || i == vec.len() {
                    //XXX: выглядит не очень
                    return rand::thread_rng().gen_range((vec.len()-2) as f64 + self.min_x..=(vec.len()-1) as f64 + self.min_x);
                } else {
                    return rand::thread_rng().gen_range(i as f64 + self.min_x..(i+1) as f64 + self.min_x);
                }
            }
        }
        unreachable!("Цикл сверху должен найти участок, выбранный случ. величиной");
    }
    fn print_hist (&self, _size: u32) { // построение гистограммы
        let mut curr_x = self.min_x;
        // FIXME: выводит дополнительную запятую в конце
        for i in 0..self.freqs.len() {
            // hist.push((curr_x, *i as f64/self.delta));
            print!("({},{}), ", curr_x, self.freqs[i]/self.delta);
            curr_x = self.min_x + self.delta*(i+1) as f64;
            // hist.push((curr_x, *i as f64/self.delta));
            print!("({},{}), ", curr_x, self.freqs[i]/self.delta);
        }
        if (curr_x - self.max_x).abs() > 0.0001 {
            panic!("(curr_x - self.max_x).abs() > 0.0001 assertion failed\n
            curr_x == {}\nself.max_x == {}\n", curr_x, self.max_x);
        }
    }
}
