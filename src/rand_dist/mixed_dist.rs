use crate::rand_dist::*;

#[derive(Debug)]
pub struct MixedDist {
    d1: StdDist,
    d2: StdDist,
    p: f64,
}

impl MixedDist {
    pub fn new (mu1: f64, lamda1: f64, nu1: f64,
                mu2: f64, lamda2: f64, nu2: f64,
                p: f64) -> MixedDist
    {
        if lamda1 <= 0. || nu1 <= 0. || nu1 >= 1.||
           lamda2 <= 0. || nu2 <= 0. || nu2 >= 1. {
            panic!("Неверные входные данные для смеси распределений");
        }
        MixedDist {
            d1: StdDist::new(mu1, lamda1, nu1),
            d2: StdDist::new(mu2, lamda2, nu2),
            p,
        }
    }
}

impl RandDist for MixedDist {
    fn pdf (&self, x: f64) -> f64 {
        println!("f1 = {}", self.d1.pdf(x));
        println!("f2 = {}", self.d2.pdf(x));
        (1.-self.p) * self.d1.pdf(x) + self.p * self.d2.pdf(x)
    }
    fn ev (&mut self) -> f64 {

        (1.-self.p) * self.d1.ev() + self.p * self.d2.ev()
    }
    fn variance (&mut self) -> f64 {
        // println!("d1.ev = {}", self.d1.variance());
        // println!("{}", self.d2.variance());
          (1.-self.p) * (self.d1.ev().powi(2) + self.d1.variance())
        + self.p      * (self.d2.ev().powi(2) + self.d2.variance())
        - ((1.-self.p) * self.d1.ev() + self.p * self.d2.ev()).powi(2)
    }
    fn assim (&mut self) -> f64 {
        let t1: f64 = (self.d1.ev() - self.ev()).powi(3) + 3. * (self.d1.ev() - self.ev()) * self.d1.variance();
        let t2: f64 = (self.d2.ev() - self.ev()).powi(3) + 3. * (self.d2.ev() - self.ev()) * self.d2.variance();

        ((1.-self.p) * t1 + self.p * t2)/self.variance().powf(1.5)
    }
    fn eks (&mut self) -> f64 {
        // println!("eks1 = {}", self.d1.eks());
        // println!("eks2 = {}", self.d2.eks());

        let t1: f64 = (self.d1.ev() - self.ev()).powi(4) + 6. * (self.d1.ev() - self.ev()).powi(2) * self.d1.variance()
                     + self.d1.variance().powi(2)*self.d1.eks();
        let t2: f64 = (self.d2.ev() - self.ev()).powi(4) + 6. * (self.d2.ev() - self.ev()).powi(2) * self.d2.variance()
                     + self.d2.variance().powi(2)*self.d2.eks();

        ((1.-self.p) * t1 + self.p * t2)/self.variance().powi(2) - 3.
    }
    fn get_rand (&self) -> f64 {
        assert_eq!(self.d1.get_lamda(), 1.);
        assert_eq!(self.d2.get_lamda(), 1.);
        let r: f64 = get_rand_exl(0., 1.);
        if r > self.p {
            return self.d1.get_rand();
        } else {
            return self.d2.get_rand();
        }
    }
    fn print_hist (&self, size: u32) {
        let mut sample: Vec<f64> = Vec::new();
        // vector<double> sample;

        for _i in 1..size {
            sample.push(self.get_rand());
        }

        let emp = EmpiricDist::new(sample);

        emp.print_hist(0);
    }
}
