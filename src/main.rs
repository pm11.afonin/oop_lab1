pub mod rand_dist;
use rand_dist::*;

fn menu () {
    println!("Режимы работы:");
    println!("1) Стандартное распределение \n\
              2) Смесь распределений \n\
              3) Эмпирическое распределение \n\
              Выберите режим [1/2/3]: ");
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).unwrap ();

    match input.trim() {
        "1" => {
            println!("Введите μ, λ, ν через пробел: ");
            input.clear();
            std::io::stdin().read_line(&mut input).unwrap ();
            let vec: Vec<f64> = input.split_whitespace().map(|word| word.parse().unwrap()).collect();

            let st = StdDist::new(vec[0], vec[1], vec[2]);
            dist_operations(Box::new(st));
        },
        "2" => {
            println!("Введите μ1, λ1, ν1, μ2, λ2, ν2, p через пробел: ");
            input.clear();
            std::io::stdin().read_line(&mut input).unwrap ();
            let vec: Vec<f64> = input.split_whitespace().map(|word| word.parse().unwrap()).collect();
            let st = MixedDist::new(vec[0], vec[1], vec[2], vec[3], vec[4], vec[5], vec[6]);
            dist_operations(Box::new(st));
        },
        "3" => {
            println!("Введите выборку, разделяя значения пробелом:");
            input.clear();
            // std::io::stdin().read_line(&mut input).unwrap ();
            // let vec: Vec<f64> = input.split_whitespace().map(|word| word.parse().unwrap()).collect();

            // let vec: Vec<f64> = vec![-1., -0.75, -0.6, -0.3, -0.2, 0.1, 0.2, 0.4, 0.7, 0.8, 1.];
            let vec: Vec<f64> = vec![-2.319075676103472, -1.6520973999164545, -1.59735224883108, -0.868570333120297, -0.679120507690185, -0.40099789762922583, -0.1550352058169601, -0.0204460599913614, 0.2846016582104201, 5.0587069369937079];
            let imp = EmpiricDist::new (vec);
            dist_operations (Box::new(imp));
        },
        _ => {
            println!("Неверный номер режима работы.");
            return;
        },
    }
}

fn dist_operations (mut dist: Box<dyn RandDist>) { //dynamic dispatch
    println!("Работа с распределение:\n\
              1) Найти f(x)\n\
              2) Найти коэф. эксцесса\n\
              3) Найти коэф. ассиметрии\n\
              4) Моделирование случ. величины\n\
              5) Построить гистограмму");
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).unwrap ();
    match input.trim() {
        "1" => {
            println!("Введите коорд. x: ");
            input.clear();
            std::io::stdin().read_line(&mut input).unwrap ();
            let x: f64 = input.trim().parse().unwrap();
            println!("f({}) = {}", x, dist.pdf(x));
        },
        "2" => {
            println!("ɣ1 = {}", dist.assim());
        },
        "3" => {
            println!("ɣ2 = {}", dist.eks());
        },
        "4" => {
            println!("Введите кол-во генерируемых чисел: ");
            input.clear();
            std::io::stdin().read_line(&mut input).unwrap ();
            let x: u32 = input.trim().parse().unwrap();
            for _i in 0..x-1 {
                print!("{}, ", dist.get_rand());
            }
            println!("{}", dist.get_rand());
        },
        "5" => {
            println!("Введите кол-во точек для построения гистограммы: ");
            input.clear();
            std::io::stdin().read_line(&mut input).unwrap ();
            let x: u32 = input.trim().parse().unwrap();
            println!("Введённое число {}", x);

            dist.print_hist(x);
        },
        _ => {
            println!("Неверный номер режима работы.");
            return;
        },
    }
}

fn main () -> () {
    menu ();
}

#[cfg(test)]
mod tests {
    use crate::RandDist;

    #[test]
    fn test_empiric_dist() {
        let vec: Vec<f64> = vec![-1., -0.75, -0.6, -0.3, -0.2, 0.1, 0.2, 0.4, 0.7, 0.8, 1.];
        let mut emp = crate::EmpiricDist::new(vec);

        // let freq_sum: f64 = emp.freqs.iter().sum();
        // assert!((freq_sum - 1).abs() < 0.0001);
        assert!((emp.pdf(-0.8) - 0.5454).abs() < 0.001);
        assert!((emp.pdf(-0.3) - 0.3636).abs() < 0.001);
        assert!((emp.pdf(0.4) - 0.5454).abs() < 0.001);
        assert!((emp.pdf(0.8) - 0.5454).abs() < 0.001);
        assert!((emp.ev() - 0.0318).abs() < 0.0001);
        assert!((emp.variance() - 0.3983).abs() < 0.0001);
        println!("{}", emp.assim());
        assert!((emp.assim() + 0.0591).abs() < 0.0001);
        println!("{}", emp.eks());
        assert!((emp.eks() + 1.2082).abs() < 0.0001);
        let _test_var = emp.get_rand ();
    }

    #[test]
    fn test_standard_dist() {
        // из методов
        let mut sd = crate::StdDist::new (0., 1., 0.2);
        assert!((sd.variance() - 11.53).abs() < 0.01);
        assert!((sd.eks() - 2.99).abs() < 0.01);
        assert!((sd.p_big() - 0.079).abs() < 0.001);
        assert!((sd.pdf(0.) - 0.200).abs() < 0.001);
        let _test_var = sd.get_rand ();

        let mut sd = crate::StdDist::new (0., 1., 0.75);
        assert!((sd.variance() - 0.33).abs() < 0.01);
        assert!((sd.eks() - 0.56).abs() < 0.01);
        assert!((sd.p_big() - 0.827).abs() < 0.001);
        assert!((sd.pdf(0.) - 0.678).abs() < 0.001);
        let _test_var = sd.get_rand ();

        let mut sd = crate::StdDist::new (0., 1., 1.);
        assert!((sd.variance() - 0.2).abs() < 0.01);
        assert!((sd.eks() + 0.86).abs() < 0.01);
        assert!((sd.p_big() - 1.).abs() < 0.001);
        assert!((sd.pdf(0.) - 0.750).abs() < 0.001);
        let _test_var = sd.get_rand ();
        //конец из методов

        let mut sd = crate::StdDist::new (0., 1.5, 0.2);
        assert!((sd.variance() - 25.95).abs() < 0.01);
        // нету данных
        // assert!((sd.eks() - 2.99).abs() < 0.01);
        // assert!((sd.p_big() - 0.079).abs() < 0.001);
        assert!((sd.pdf(0.) - 0.133).abs() < 0.001);

        let mut sd = crate::StdDist::new (2., 1., 0.2);
        assert!((sd.variance() - 11.53).abs() < 0.01);
        assert!((sd.eks() - 2.99).abs() < 0.01);
        assert!((sd.p_big() - 0.079).abs() < 0.001);
        assert!((sd.pdf(0.) - 0.091).abs() < 0.001);
        let _test_var = sd.get_rand ();
    }

    #[test]
    fn test_print_standard_dist() {
        // из методов
        let mut sd = crate::StdDist::new (0., 1., 0.2);
        println!("{}", sd.variance());
        println!("{}", sd.eks());
        println!("{}", sd.p_big());
        println!("{}", sd.pdf(0.));

        let mut sd = crate::StdDist::new (0., 1., 0.3);
        println!("{}", sd.variance());
        println!("{}", sd.eks());
        println!("{}", sd.p_big());
        println!("{}", sd.pdf(0.));

        let mut sd = crate::StdDist::new (0., 1., 0.4);
        println!("{}", sd.variance());
        println!("{}", sd.eks());
        println!("{}", sd.p_big());
        println!("{}", sd.pdf(0.));

        let mut sd = crate::StdDist::new (0., 1., 0.5);
        println!("{}", sd.variance());
        println!("{}", sd.eks());
        println!("{}", sd.p_big());
        println!("{}", sd.pdf(0.));

        let mut sd = crate::StdDist::new (0., 1., 0.6);
        println!("{}", sd.variance());
        println!("{}", sd.eks());
        println!("{}", sd.p_big());
        println!("{}", sd.pdf(0.));

        let mut sd = crate::StdDist::new (0., 1., 0.7);
        println!("{}", sd.variance());
        println!("{}", sd.eks());
        println!("{}", sd.p_big());
        println!("{}", sd.pdf(0.));

        let mut sd = crate::StdDist::new (0., 1., 0.75);
        println!("{}", sd.variance());
        println!("{}", sd.eks());
        println!("{}", sd.p_big());
        println!("{}", sd.pdf(0.));

        let mut sd = crate::StdDist::new (0., 1., 0.8);
        println!("{}", sd.variance());
        println!("{}", sd.eks());
        println!("{}", sd.p_big());
        println!("{}", sd.pdf(0.));

        let mut sd = crate::StdDist::new (0., 1., 0.9);
        println!("{}", sd.variance());
        println!("{}", sd.eks());
        println!("{}", sd.p_big());
        println!("{}", sd.pdf(0.));

        let mut sd = crate::StdDist::new (0., 1., 1.);
        println!("{}", sd.variance());
        println!("{}", sd.eks());
        println!("{}", sd.p_big());
        println!("{}", sd.pdf(0.));
        //конец из методов

        println!("Остальные тесты");
        let mut sd = crate::StdDist::new (0., 1., 0.2);
        println!("{}", sd.pdf(0.));
        println!("{}", sd.ev());
        println!("{}", sd.variance());
        println!("{}", sd.assim ());
        println!("{}", sd.eks());
        println!("{}", sd.p_big());

        println!();

        let mut sd = crate::StdDist::new (0.6, 2., 0.3);
        println!("{}", sd.pdf(0.));
        println!("{}", sd.ev());
        println!("{}", sd.variance());
        println!("{}", sd.assim ());
        println!("{}", sd.eks());
        println!("{}", sd.p_big());

        panic!("End.");
    }

    #[test]
    fn test_print_mixed_dist() {
        let mut mx = crate::MixedDist::new (2., 2., 0.4, 4., 2., 0.4, 0.25);
        println!("{}", mx.pdf(0.));
        println!("{}", mx.ev());
        println!("{}", mx.variance());
        println!("{}", mx.assim ());
        println!("{}", mx.eks());

        println!();

        let mut mx = crate::MixedDist::new (2., 0.3, 0.15, 4., 0.6, 0.75, 0.5);
        println!("{}", mx.ev());
        println!("{}", mx.eks());

        println!();

        let mut mx = crate::MixedDist::new (0., 1., 0.8, 0., 3., 0.8, 0.5);
        println!("{}", mx.variance());

        panic!("End.");
    }

    #[test]
    fn test_print_empiric_dist() {
        println!("Стандартное нормальное с параметрами:");
        let mut sd = crate::StdDist::new (0., 1., 0.41);
        get_emp_vals (&sd, 700);
        let mut sd = crate::StdDist::new (0., 1., 0.41);
        get_emp_vals (&sd, 70_000);
        let mut sd = crate::StdDist::new (0., 1., 0.41);
        get_emp_vals (&sd, 7_000_000);

        println!("Стандартное нормальное с параметрами:");
        let mut sd = crate::StdDist::new (7., 1., 0.41);
        get_emp_vals (&sd, 700);
        let mut sd = crate::StdDist::new (7., 1., 0.41);
        get_emp_vals (&sd, 70_000);
        let mut sd = crate::StdDist::new (7., 1., 0.41);
        get_emp_vals (&sd, 7_000_000);

        println!("Смесь распределений:");
        let mut mx = crate::MixedDist::new (3., 1., 0.76, 5., 1., 0.48, 0.8);
        let eeekss = mx.eks();
        println!("test_eks = {}", eeekss);
        get_emp_vals (&mx, 700);
        let mut mx = crate::MixedDist::new (3., 1., 0.76, 5., 1., 0.48, 0.8);
        get_emp_vals (&mx, 70_000);

        panic!("End.");
    }

    fn get_emp_vals (dist: &impl RandDist, size: u32) {
        let mut sample: Vec<f64> = Vec::new();
        for _i in 1..size {
            sample.push(dist.get_rand());
        }
        let mut emp = crate::EmpiricDist::new(sample);
        println!("Выборка размера: {}", size);
        println!("{}", emp.ev());
        println!("{}", emp.variance());
        println!("{}", emp.assim ());
        println!("{}", emp.eks());
        dist.print_hist(size);
        if size == 7_000_000 {
            println!("Гистограмма и хар-ки выборки эмп распеделения: ");

            let mut sample: Vec<f64> = Vec::new();
            for _i in 1..size {
                sample.push(emp.get_rand());
            }
            let mut emp2 = crate::EmpiricDist::new(sample);
            println!("{}", emp2.ev());
            println!("{}", emp2.variance());
            println!("{}", emp2.assim ());
            println!("{}", emp2.eks());
            emp2.print_hist(size);
        }
    }

    #[test]
    fn test_mixed_dist() {
        let mut mx = crate::MixedDist::new (2., 1.5, 0.5, 3., 2.5, 0.7, 0.75);
        assert!((mx.ev() - 2.75).abs() < 0.001);
        println!("{}", mx.variance());
        assert!((mx.variance() - 2.78461).abs() < 0.00001);
        assert!((mx.assim() + 0.03882).abs() < 0.00001);
        assert!((mx.eks() + 4.88306).abs() < 0.0001);
        println!("{}", mx.pdf(0.5));
        assert!((mx.pdf(0.5) - 0.075).abs() < 0.001);
        // panic!("{}", mx.eks());

        let test_var = mx.get_rand ();
    }

    // #[test]
    // fn another() {
    //     panic!("Make this test fail");
    // }
}
